/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logging;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author mage
 */
public class myLogger {
    public static final Logger LOGGER = Logger.getLogger("DigraphLogger");
    private static myLogger myLoggerInstance = null;
    
    public static myLogger getLoggerInstance(){
        if(myLoggerInstance == null){
            LOGGER.setLevel(Level.INFO);    // Only modify this to change log level
            
            LOGGER.setUseParentHandlers(false);
            ConsoleHandler handler = new ConsoleHandler();
            handler.setLevel(Level.FINEST);
            handler.setFormatter(new SimpleFormatter());
            LOGGER.addHandler(handler);
            myLoggerInstance = new myLogger();
        }
        return myLoggerInstance;
    }
}
