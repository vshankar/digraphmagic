package graph;

import logging.myLogger;

/**
 *
 * @author mage
 */
public class DigraphRunner {
    static myLogger log = myLogger.getLoggerInstance();
    
    public static void main(String[] args) {
        
        // Define variable inputs
        String input = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
        
        Digraph D = new Digraph(input);
        log.LOGGER.info(D.toString());
        // Q1-5
        
        log.LOGGER.info("Distance of route A-B-C: " + D.routeDistance("A-B-C"));
        log.LOGGER.info("Distance of route A-D: " + D.routeDistance("A-D"));
        log.LOGGER.info("Distance of route A-D-C: " + D.routeDistance("A-D-C"));
        log.LOGGER.info("Distance of route A-E-B-C-D: " + D.routeDistance("A-E-B-C-D"));
        log.LOGGER.info("Distance of route A-E-D: " + D.routeDistance("A-E-D"));
        // Q6
        log.LOGGER.info("Number of different routes between C-C, max 3: " + 
                D.enumerateRoutes("C", "C", 3, "max"));
        // Q7
        log.LOGGER.info("Number of different routes between A-C, exactly 4: " + 
                D.enumerateRoutes("A", "C", 4, "exactly"));
        // Q8
        log.LOGGER.info("Length of the shortest path between A and C is : " + D.djikstra("A", "C"));
        // Q9
        log.LOGGER.info("Length of the shortest path between B and B is : " + D.djikstra("B", "B"));
        // Q10
        log.LOGGER.info("Number of paths with length less than 30 between C and C: " + D.dfs("C", "C", 30));
    }
    
}
