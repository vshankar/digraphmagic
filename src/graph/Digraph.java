/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

import logging.myLogger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import static graph.HelperFunctions.*;
import java.util.logging.Level;

/**
 *
 * @author mage
 */
public class Digraph{
    int V;
    int E;
    int adjMat[][];
    HashMap<String, Node> nodeSet = new HashMap<String, Node>();
    int defaultEdgeWeight = 0;
    myLogger log = myLogger.getLoggerInstance();
    
    /**
     * Constructs a digraph
     * @param initGraph string of the format 'EiEjWt, EkElWt...'
     */
    public Digraph(String initGraph){
        String[] edgeList = initGraph.split("\\s*,\\s*");
        List<Edge> edges = new ArrayList<>();
        int i = 0;
        for(String edgeString : edgeList){
            // Add edge to my list of edges (constructor checks for input sanity
            try{
                Edge edge = new Edge(edgeString);
                // Checks for node value i.e. the index of the node
                if(!nodeSet.containsKey(edge.getStartNode().getNodeLabel())){
                    edge.getStartNode().setNodeValue(i);
                    nodeSet.put(edge.getStartNode().getNodeLabel(), edge.getStartNode());
                    i++;
                }else
                    edge.getStartNode().setNodeValue(nodeSet.get(edge.getStartNode().getNodeLabel()).getNodeValue());
                if(!nodeSet.containsKey(edge.getEndNode().getNodeLabel())){
                    edge.getEndNode().setNodeValue(i);
                    nodeSet.put(edge.getEndNode().getNodeLabel(), edge.getEndNode());
                    i++;
                }else
                    edge.getEndNode().setNodeValue(nodeSet.get(edge.getEndNode().getNodeLabel()).getNodeValue());
                edges.add(edge);
            }catch(Exception e){
                log.LOGGER.severe(e + "\nPlease fix the string input before continuing");
                System.exit(1);
            }
        }
        this.V = nodeSet.size();
        this.E = edges.size();
        this.adjMat = new int[V][V];
        for(Edge edge:edges){
            log.LOGGER.info("Trying to add edge: " + edge.toString());
            // adds edges to the adjcaceny matrix
            addEdge(edge.getStartNode(), edge.getEndNode(), edge.getWeight());
        }
        log.LOGGER.info("Input Graph" + "::" + toString());
    }
    
    /**
     * Adds an edge to the adjacency matrix of the digraph
     * @param n1 start node of the edge
     * @param n2 end node of the edge
     * @param weight weight between the two nodes
     */
    void addEdge(Node n1, Node n2, int weight){
        adjMat[n1.getNodeValue()][n2.getNodeValue()] = weight;
    }
    
    /**
     * Returns edge weight (length of route) between two nodes 
     * @param V1 start node of the edge
     * @param V2 end node of the edge
     * @return weight of the edge between the two nodes
     */
    public int getEdgeWeight(String V1, String V2){
        if(nodeSet.containsKey(V1) && nodeSet.containsKey(V2))
            return adjMat[nodeSet.get(V1).getNodeValue()][nodeSet.get(V2).getNodeValue()];
        else
            return -1;
    }

    /**
     * Returns edge weight (length of route) between two nodes from a given matrix 
     * @param V1 start node of the edge
     * @param V2 end node of the edge
     * @param adjMat adjacency matrix
     * @return weight of the edge between the two nodes
     */
    public int getEdgeWeight(String V1, String V2, int adjMat[][]){
        if(nodeSet.containsKey(V1) && nodeSet.containsKey(V2))
            return adjMat[nodeSet.get(V1).getNodeValue()][nodeSet.get(V2).getNodeValue()];
        else
            return -1;
    }
    
    /**
     * Sets edge weight (length of route) between two nodes in an adjacency matrix
     * @param V1 start node of the edge
     * @param V2 end node of the edge
     * @param weight weight of the edge to be set
     */
    public void setEdgeWeight(String V1, String V2, int adjMat[][], int weight){
        if(nodeSet.containsKey(V1) && nodeSet.containsKey(V2))
            adjMat[nodeSet.get(V1).getNodeValue()][nodeSet.get(V2).getNodeValue()] = weight;
    }
    
    public String toString(int mat[][]) {
        StringBuilder s = new StringBuilder();
        s.append(mat.length + " nodes\n");
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                s.append(mat[i][j] + " ");
            }
            s.append("\n");
        }
        return s.toString();
    }
    
    public String toString() {
        String nodes[] = new String[nodeSet.keySet().size()];
        for(String node: nodeSet.keySet())
            nodes[nodeSet.get(node).getNodeValue()] = node;
        StringBuilder s = new StringBuilder();
        s.append(V + " nodes " + E + " edges\n");
        s.append(Arrays.toString(nodes) +"\n");
        for (int i = 0; i < adjMat.length; i++) {
            s.append(nodes[i]);
            for (int j = 0; j < adjMat[i].length; j++) {
                s.append(adjMat[i][j] + " ");
            }
            s.append("\n");
        }
        return s.toString();
    }

    /**
     * Converts weighted adjacency matrix to binary where 1 indicates an edge 
     * and 0 indicates no edge
     * @param adjMat 2D adjacency matrix
     * @return binarized version of the weighted adjacecny matrix
     */
    private int[][] convertToEdgePresence(int[][] adjMat) {
        int presenceMat[][] = new int[adjMat.length][adjMat[0].length];
        for(int i = 0; i<adjMat.length; i++)
            for(int j = 0; j<adjMat[0].length; j++)
                if(adjMat[i][j]>0)
                    presenceMat[i][j] = 1;
        return presenceMat;
    }
    
    /**
     * Computes and returns the distance along a given route
     * @param route String of the format V1-V2-V3-.... where Vi is a single alphabet
     * @return int distance of traversing the whole route
     */
    String routeDistance(String route) {
        // check if the string input is in the desired format
        //TODO
        // split up the route string into the node list
        List<String> nodeList = Arrays.asList(route.split("\\s*-\\s*"));
        int distance = 0;
        boolean routeValid = true;
        if(nodeSet.keySet().containsAll(nodeList)){
            for(int i=0; i<(nodeList.size()-1)&&routeValid; i++){
                int wt = getEdgeWeight(nodeList.get(i), nodeList.get(i+1));
                if(wt>0)
                   distance+=wt;
                else
                    routeValid = false;
            }
        }else{
            Exception e = new IllegalArgumentException("Input String format is not valid: " + 
                        route + ". Should look like A-B-C or A-D."
                    + "\nMake sure that the input contains nodes present in the graph");
            log.LOGGER.severe(e.toString());
            routeValid = false;
        }
        if(routeValid)
            return Integer.toString(distance);
        else
            return "NO SUCH ROUTE";
    }

    /**
     * Enumerates the number of different routes between two nodes using matrix
     * power operations
     * @param sourceNode String beginning node of route
     * @param targetNode String ending node of route
     * @param K int number of stops
     * @param ul String one of 'max' or 'exactly'
     * @return int number of routes between two nodes
     */
    public int enumerateRoutes(String sourceNode, String targetNode, int K, String ul) {
        int binaryMat[][] = convertToEdgePresence(adjMat);
        int walkMat[][] = binaryMat;
        int powerMat[][] = walkMat;
        if(K<0){
            log.LOGGER.severe("The number of stops should not be negative");
            return -1;
        }
        // check whether source node and end node are part of the graph
        if(!(nodeSet.containsKey(sourceNode) && nodeSet.containsKey(targetNode))){
            log.LOGGER.severe("The source and/or target node are not part of "
                    + "the graph. Terminating enumeration...");
            return -1;
        }
        switch(ul){
            case "exactly":
                for(int i = 0; i<(K-1); i++){
                    walkMat = matrixMult(walkMat, binaryMat);
                    log.LOGGER.fine("Walkmat" + i + "::" + toString(walkMat));
                }
                break;
            case "max":
                for(int i = 0; i<(K-1); i++){
                    powerMat = matrixMult(powerMat, binaryMat);
                    walkMat = matrixSum(powerMat, walkMat);
                    log.LOGGER.fine("Powermat" + i + "::" + toString(walkMat));
                }
                break;
            default:
                log.LOGGER.severe("argument #4 should be one of \"max\" or \"exactly\"");
                return -1;
        }
        int numRoutes = getEdgeWeight(sourceNode, targetNode, walkMat);
        if(numRoutes<0)
            log.LOGGER.warning("Something went wrong and the source and/or "
                    + "target node are not part of the graph");
        return(numRoutes);
    }

    /**
     * Runs Djikstra's to compute SSSP
     * @param sourceNode source node to compute shortest paths from
     * @param endNode end node to return the shortest path to
     * @return returns the shortest path found between the source and end nodes
     */
    public int djikstra(String sourceNode, String endNode){
        // check whether source node and end node are part of the graph
        if(!(nodeSet.containsKey(sourceNode) && nodeSet.containsKey(endNode))){
            log.LOGGER.severe("The source and/or target node are not part of "
                    + "the graph. Terminating djikstra's...");
            return -1;
        }
        // define variables
        int u,i=0;
        int djikstraAdjMat[][];
        if(sourceNode.equals(endNode)){
            // trying to find the shortest path of a node to itself
            log.LOGGER.fine("Computing shortest path of node to itself. Will "
                    + "modify the adjacency matrix by splitting the node");
            djikstraAdjMat = splitOneNode(adjMat, nodeSet.get(sourceNode).getNodeValue());
        }
        else
            djikstraAdjMat = splitOneNode(adjMat, -1);  // just returns a copy of the adjMat
        int numNodes = djikstraAdjMat.length;
        log.LOGGER.log(Level.FINE, "Djikstra adjacency Mat : {0}", toString(djikstraAdjMat));
        // define a cost array and set the source weight to 0
        Double costMat[] = new Double[numNodes];
        Arrays.fill(costMat, Double.POSITIVE_INFINITY);
        costMat[nodeSet.get(sourceNode).getNodeValue()] = (double)0;
        // set of visited nodes
        int S[] = new int[numNodes];
        // array of nodes yet to be explored
        List<Integer> Q = new ArrayList<Integer>();
        for( i = 0; i<numNodes; i++)
            Q.add(i);
        while(Q.size()>0){
            u = extractMin(Q, costMat);
            Q.remove(Integer.valueOf(u));
            S[u] = 1;
            
            // for each v, neighbour of u, relax the edge (u,v)
            for(int v=0; v<numNodes; v++){
                    // relaxation
                    if(djikstraAdjMat[u][v]>0)
                        if(costMat[v] > costMat[u] + djikstraAdjMat[u][v])
                            costMat[v] = costMat[u] + djikstraAdjMat[u][v];
            }
        }
        if(sourceNode.equals(endNode))
            return costMat[costMat.length-1].intValue();
        else
            return costMat[nodeSet.get(endNode).getNodeValue()].intValue();
    }
    
    /**
     * Depth first traversal of graph
     * @param startNode node to begin dfs at
     * @param endNode node to compute the number of paths to
     * @param lengthMax maximum length/distance of every path allowed
     * @return number of paths (where paths allow repitition of nodes) between 
     * startNode and endNode of length at most lengthMax
     */
    public int dfs(String startNode, String endNode, int lengthMax){
        int length = 0;
        int numRoutes[] = {0};
        // check whether source node and end node are part of the graph
        if(!(nodeSet.containsKey(startNode) && nodeSet.containsKey(endNode))){
            log.LOGGER.severe("The source and/or target node are not part of "
                    + "the graph. Terminating dfs...");
            return -1;
        }
        if(lengthMax<0){
            log.LOGGER.severe("The length/distance of the paths should not be negative");
            return -1;
        }
        int startNodeValue = nodeSet.get(startNode).getNodeValue();
        int endNodeValue = nodeSet.get(endNode).getNodeValue();
        dfsVisit(startNodeValue, endNodeValue, length, lengthMax, numRoutes);
        return(numRoutes[0]);
    }
    
    /**
     * Recursively called method helper for dfs
     * @param startNodeValue index of the node to be visited
     * @param endNodeValue index of the node that terminates the path
     * @param length current length/distance of the path found
     * @param lengthMax maximum allowed length/distance of allowed paths
     * @param numRoutes counter that maintains number of allowed paths discovered
     */
    public void dfsVisit(int startNodeValue, int endNodeValue, int length, int lengthMax, int[] numRoutes){
        int newLength = 0;
        log.LOGGER.log(Level.FINE, "Starting at node {0}", startNodeValue);
        for(int i = 0; i<nodeSet.size(); i++){
            if(adjMat[startNodeValue][i]>0){
                newLength = length + adjMat[startNodeValue][i];
                if(newLength<lengthMax){
                    if(i==endNodeValue){
                        log.LOGGER.log(Level.FINE, "YAY! 1 path discovered of length={0}", newLength);
                        numRoutes[0]++;
                    }
                    log.LOGGER.log(Level.FINE, "Visiting {0}. Length={1}", new Object[]{i, newLength});
                    dfsVisit(i, endNodeValue, newLength, lengthMax, numRoutes);
                }
                else
                    log.LOGGER.fine("Ended");
            }
        }
    }
}


    