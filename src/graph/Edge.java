package graph;


import graph.Node;

/**
* Convenience class to represent edges
* @author mage
*/
public class Edge{
   int weight; 
   Node N1, N2;

   public Edge(Node n1, Node n2){
       this.N1 = n1;
       this.N2 = n2;
       this.weight = -1;    // this isnt allocated now
   }

   public Edge(String edgeString){
       if(isInputEdgeSane(edgeString)){
           this.N1 = new Node(edgeString.substring(0, 1));
           this.N2 = new Node(edgeString.substring(1, 2));
           this.weight = convertWeightToInt(edgeString.substring(2));
       }else{
           throw new IllegalArgumentException("Input String format is not valid: " + 
                   edgeString + ". Should look like AB4");
       }
   }

   int convertWeightToInt(String nodeWeight){
       int wt = Integer.valueOf(nodeWeight);
       return wt;
   }

   boolean isInputEdgeSane(String edgeString){
       boolean isValid = true;
       if(!(Character.isLetter(edgeString.charAt(0)) && Character.isLetter(edgeString.charAt(1))))
           isValid = false;
       if(edgeString.charAt(0)==edgeString.charAt(1))
           isValid = false;        // no self-loops
       try{
           int i = Integer.valueOf(edgeString.substring(2));
           if(i<=0)
               isValid = false;
       }catch(NumberFormatException e){
           isValid = false;
       }
       return isValid;
   }

   public Node getStartNode(){
       return N1;
   }

   public Node getEndNode(){
       return N2;
   }

   public int getWeight(){
       return weight;
   }

   public String toString(){
       StringBuilder s = new StringBuilder();
       s.append(N1.toString() + " -> " + N2.toString() + ":" + weight);
       return s.toString();
   }
}
