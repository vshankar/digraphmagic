package graph;

import java.util.logging.Level;
import logging.myLogger;

/**
 * Class to store and operate on nodes of a graph
 * @author mage
 */
public class Node {
    int V;
    String vLabel;
    Double distance;
    myLogger log = myLogger.getLoggerInstance();

    public Node(){
        this.V = 0;
        this.vLabel = "";
        this.distance = (double)0;
    }
    public Node(String v){
        this.V = -1;
        this.vLabel = v;
        this.distance = Double.POSITIVE_INFINITY;
        log.LOGGER.log(Level.FINE, "Set node with dets: {0}:{1}:{2}", new Object[]{V, vLabel, distance});
    }
    
    public String getNodeLabel(){
        return vLabel;
    }
    public int getNodeValue(){
        return V;
    }
    public void setNodeValue(int nodeValue){
        this.V = nodeValue;
    }
    public Double getDistance(){
        return distance;
    }
    public void setDistance(Double d){
        this.distance = d;
    }
    public String toString(){
        return vLabel;
    }
}
