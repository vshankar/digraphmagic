
package graph;

import logging.myLogger;
import java.util.List;

/**
 *
 * @author mage
 */
public class HelperFunctions {
    myLogger log = myLogger.getLoggerInstance();
    
    /**
     * Convenience method for matrix multiplication
     * @param M1 1st 2D array
     * @param M2 2nd 2D array
     * @return product of the two matrices
     */
    public static int[][] matrixMult(int[][] M1, int[][] M2) {
    int rowsM1 = M1.length;
    int colsM1 = M1[0].length;
    int colM2 = M2[0].length;
    int product[][] = new int[rowsM1][colM2];
    // check if #cols_matrix1 == #row_matrix2
    if ((M1[0].length == M2.length)) {
        for (int i = 0; i < rowsM1; i++) {
            for (int j = 0; j < colM2; j++) {
                for(int k=0; k < colsM1; k++){
                    product[i][j] += M1[i][k] * M2[k][j];
                }
            }
        }
    }
    return product;
  }

    /**
     * Convenience method for matrix addition
     * @param M1 1st 2D array
     * @param M2 2nd 2D array
     * @return sum of the two matrices
     */
    public static int[][] matrixSum(int[][] M1, int[][] M2) {
        int sumMat[][] = new int[M1.length][M1[0].length];
        // check if #cols_matrix1 == #cols_matrix2
        // check if #rows_matrix1 == #rows_matrix2
        if ((M1[0].length == M2[0].length) && (M1.length == M2.length)) {
            for(int i=0; i<M1.length; i++)
                for(int j = 0; j<M1[0].length; j++)
                    sumMat[i][j] = M1[i][j] + M2[i][j];
        }
        return sumMat;
    }
    
    /**
     * Extracts the integer with the minimum value from a 2D array amongst 
     * the list of allowed nodes 
     * @param Q list that contains the indices of nodes
     * @param costMat array containing a cost associated with node indices
     * @return index of the node with the minimum value in costMat amongst
     * those present in list of nodes Q
     */
    public static int extractMin(List<Integer> Q, Double costMat[]){
        int minNode = 0;
        Double min = Double.POSITIVE_INFINITY;
        for(int i=0; i<Q.size(); i++) {
            if(Q.get(i)<costMat.length){    // safety check ...shouldn't ever fail
                if(costMat[Q.get(i)] <= min){
                    minNode = Q.get(i);
                    min = costMat[minNode];
                }
            }
        }
        return(minNode);
    }
    
    /**
     * Splits a given node in an adjacency matrix into two
     * The original node has all the out-edges
     * The new node has all the in-edges
     * @param source
     * @param indexToSplit
     * @return 
     */
    public static int[][] splitOneNode(int[][] source, int indexToSplit) {
        int copyTillRows = source.length;
        int copyTillCols = source[0].length;
        int numRows = copyTillRows+1;
        int numCols = copyTillCols+1;
        int[][] target;
        if(indexToSplit!=-1)
            target = new int[numRows][numCols];
        else
            target = new int[copyTillRows][copyTillCols];
        for (int i = 0; i < copyTillRows; i++) {
            System.arraycopy(source[i], 0, target[i], 0, copyTillCols);
        }
        if(indexToSplit!=-1){
            // All out-weights (down the column) for the indexToSplit to be moved
            for(int i = 0; i<copyTillRows; i++){
                target[i][numCols-1] = source[i][indexToSplit];
                target[i][indexToSplit] = 0;
            }
        }
        return target;
    }
}

